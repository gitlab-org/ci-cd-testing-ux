# What is this? 

This is a project that allows you to demo and test GitLab's CI/CD functionality.

The project itself, is a simple static HTML/CSS/JS website of a minimum wage calculator.

The goal of this project is to give anyone the ability to test the whole range of CI/CD functionalities that GitLab provides. Because of this, the project's configuration is constantly evolving and getting updated.

# How to use this project?

You can use this project to demo and test GitLab's CI/CD functionality. This project has a basic CI/CD configuration that launches a review app when a new merge request is created. You can manually stop the app or the app will get deleted once you close the merge request and the branch gets deleted.

To trigger a pipeline you just need to do a small change and create a merge request. You can change any file but we recommend changing the `demo.txt` at the root of the project. Simply create a new branch and add a new line to the `demo.txt` file with any random text (e.g: Today's date). This will trigger a pipeline that will execute succesfully in its simplest scenario.

[Click here](https://gitlab.com/-/ide/project/ci-cd-uxgroup/ci-cd-testing/edit/master/-/demo.txt) to make a new change to `demo.txt` on the Web IDE. After adding a new line of text, simply stage and commit. This will create a new change that will trigger the pipeline.


# How to setup a similar project from scratch?

If you want to create a small project like this from scratch, you can follow this quickstart guide that explains how to do it.

To accomplish everything described in this guide this you will need a `glitch.com` account and `zeit.co` account (both free). Also make sure to host your files inside a `src` folder so you can use the boilerplate `.gitlab-ci.yml` file used in the video, which you can get from this [snippet](https://gitlab.com/snippets/1928598)

[![Simple CI Configuration](http://img.youtube.com/vi/mzpPCK4s958/0.jpg)](http://www.youtube.com/watch?v=mzpPCK4s958 "Simple CI Configuration")

^^ Click thumbnail to play video